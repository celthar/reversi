package name.panitz.pmt2012;

import java.util.List;

public interface Game<M> {
    List<M> moves();
    Game<M> doMove(M m);
    boolean isFirstPlayer();
    boolean otherPlayerHasWon();
    int evalState(boolean forTheFirst);
}
