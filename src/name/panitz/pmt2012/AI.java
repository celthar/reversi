package name.panitz.pmt2012;

public interface AI<M> {
    M getBestMove(Game<M> m);
}
