/**
 *
 */
package name.panitz.pmt2012;

import java.util.List;

/**
 * @author sep
 *
 */
public class MinMax<M> implements AI<M> {
	static int DEPTH = 3;

	/* (non-Javadoc)
	 * @see name.panitz.pmt2012.AI#getBestMove(name.panitz.pmt2012.Game)
	 */
	@Override
	public M getBestMove(Game<M> g) {
		int val = -Integer.MAX_VALUE;
		M result = null;
		for (M m:g.moves()){
			if (result==null) result=m;
			final int mvEval = minmax(g.doMove(m),DEPTH,g.isFirstPlayer());
			if (mvEval>val){
				val=mvEval;
				result=m;
			}
		}
		return result;
	}

	int minmax(Game<M> g, int depth, boolean forFirstPLayer){
		List<M> moves = g.moves();
		if (depth==0
			|| g.otherPlayerHasWon()
			|| moves.isEmpty()
				) return g.evalState(forFirstPLayer);
		boolean isMax = g.isFirstPlayer()==forFirstPLayer;
		int val = isMax?-Integer.MAX_VALUE:Integer.MAX_VALUE;
		for (M m:moves){
			final int mvEval =  minmax(g.doMove(m),depth-1,forFirstPLayer);
			if (isMax && mvEval>val) val=mvEval;
			if (!isMax && mvEval<val) val=mvEval;
		}

		return val;
	}


}