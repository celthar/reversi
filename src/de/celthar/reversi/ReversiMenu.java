package de.celthar.reversi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * Diese Klasse stellt eine JMenuBar dar, die auf das ReversiGui-Objekt
 * zugeschnitten ist.
 * @author sascha
 */
public class ReversiMenu extends JMenuBar {
    private ReversiGui gui;

    /**
     * Der Konstruktor um das ReversiMenu-Objekt zu erstellen.
     * @param rGui Das ReversiGui-Objekt, welches die Menüaufrufe entgegen nimmt
     */
    public ReversiMenu(ReversiGui rGui) {
        this.gui = rGui;

        JMenu menu = new JMenu("Game");
        JMenuItem menuItem = new JMenuItem("New Game (vs Player)");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                gui.startNewGame(false);
            }
        });
        menu.add(menuItem);

        menuItem = new JMenuItem("New Game (vs Computer)");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                gui.startNewGame(true);
            }
        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Save Game");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                gui.saveGame();
            }
        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Load Game");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                gui.loadGame();
            }
        });
        menu.add(menuItem);

        this.add(menu);
    }
}
