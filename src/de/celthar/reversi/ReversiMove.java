package de.celthar.reversi;

/**
 * Ein Objekt dieser Klasse stellt einen Spielzug in dem Spiel Reversi dar
 * @author sascha
 */
public class ReversiMove {
    /**
     * Die Reihe, in die der Spielstein gesetzt werden soll
     */
    byte row;
    /**
     * Die Spalte, in die der Spielstein gesetzt werden soll
     */
    byte column;

    /**
     * Konstruktor für ein ReversiMove-Objekt
     * @param row Die Reihe, in der der Spielstein gesetzt werden soll
     * @param column Die Spalte, in der der Spielstein gesetzt werden soll
     */
    public ReversiMove(byte row, byte column) {
        this.row = row;
        this.column = column;
    }
}
