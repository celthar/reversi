package de.celthar.reversi;

import name.panitz.pmt2012.Game;
import java.util.LinkedList;
import java.util.List;

/**
 * Ein Objekt dieser Klasse stellt ein komplettes Reversi-Spiel dar. (ohne Gui)
 * @author sascha
 */
public class Reversi implements Game<ReversiMove>, Cloneable {
    /**
     * Größe des Spielfeldes. (plus 2 Felder pro Seite, vereinfacht den Suchalgorithmus)
     */
    public static final byte SIZE = 10;

    /**
     * Anfang des nutzbaren Spielfeldes.
     */
    public static final byte FIELD_BEGIN = 1;

    /**
     * Ende des nutzbaren Spielfeldes.
     */
    public static final byte FIELD_END = SIZE - 1;

    /**
     * Anfang der mittleren Felder. (für den Anfang des Spiels)
     */
    public static final byte MIDDLE_BEGIN = (SIZE / 2) - 1;

    /**
     * Ende der mittleren Felder .(für den Anfang des Spiels)
     */
    public static final byte MIDDLE_END = (SIZE / 2) + 1;

    /**
     * Mögliche Belegung eines Spielfeldes. (kein Spieler)
     */
    public static final byte NO_PLAYER = 0;

    /**
     * Mögliche Belegung eines Spielfeldes. (erster Spieler)
     */
    public static final byte FIRST_PLAYER = 1;

    /**
     * Mögliche Belegung eines Spielfeldes. (zweiter Spieler)
     */
    public static final byte SECOND_PLAYER = 2;

    /**
     * Mögliche Spielphasen. (Anfangsphase)
     */
    public static final byte PHASE_BEGIN = 0;

    /**
     * Mögliche Spielphasen. (normales Spiel)
     */
    public static final byte PHASE_NORMAL = 1;

    /**
     * boolean ob der erste Spieler an der Reihe ist oder nicht.
     */
    private boolean isFirstPlayer = true;

    /**
     * Die momentane Spielphase. (Standard: {@link PHASE_BEGIN})
     */
    private byte curPhase = PHASE_BEGIN;

    /**
     * Die Anzahl der Züge, die hintereinander gepasst wurden.
     */
    private byte passCount = 0;

    /**
     * Die Anzahl der bisher ausgeführten Züge.
     */
    private byte turnCount = 0;

    /**
     * Das Spielbrett dargestellt als 2-dimensionales Array.
     */
    public byte[][] board = new byte[SIZE][SIZE];

    /**
     * Sammelt alle momentan möglichen Spielzüge als List-Objekt.
     * @return List&lt;ReversiMove&gt; enthält alle möglichen Spielzüge
     */
    @Override
    public List<ReversiMove> moves() {
        List<ReversiMove> moveList = new LinkedList<>();

        for(byte x = FIELD_BEGIN; x < FIELD_END; x++) {
            for(byte y = FIELD_BEGIN; y < FIELD_END; y++) {
                if(board[x][y] == NO_PLAYER && validMove(x, y)) {
                    moveList.add(new ReversiMove(x, y));
                }
            }
        }

        return moveList;
    }

    /**
     * Führt einen Spielzug aus und gibt das daraus resultierende Reversi-Objekt zurück.
     * @param m Der Spielzug, der ausgeführt werden soll
     * @return Das Reversi-Objekt, nachdem der Spielzug ausgeführt wurde
     */
    @Override
    public Reversi doMove(ReversiMove m) {
        byte token = isFirstPlayer?FIRST_PLAYER:SECOND_PLAYER;

        try {
            Reversi result = (Reversi) clone();

            if(m == null) {
                result.passCount++;
            } else {
                result.turnTokens(m.row, m.column);
                result.board[m.row][m.column] = token;
                result.passCount = 0;
            }

            result.isFirstPlayer = !isFirstPlayer;
            result.turnCount++;

            if(result.turnCount >= 4) {
                result.curPhase = PHASE_NORMAL;
            }

            return result;
        } catch (CloneNotSupportedException ex) {
            throw new Error("Not possible!");
        }
    }

    /**
     * Gibt zurück, ob der erste Spieler an der Reihe ist oder nicht.
     * @return ture, wenn der erste Spieler an der Reihe ist; false, sonst
     */
    @Override
    public boolean isFirstPlayer() {
        return isFirstPlayer;
    }

    /**
     * Gibt zurück, ob der andere Spieler gewonnen hat (oder es Unentschieden steht).
     * @return true, wenn der andere Spieler gewonnen hat; false, sonst
     */
    @Override
    public boolean otherPlayerHasWon() {
        int tokenFirst = countTokens(true);
        int tokenSecond = countTokens(false);

        if(passCount >= 2 || (tokenFirst + tokenSecond) == 64) {
            if(isFirstPlayer && tokenSecond > 32) {
                System.out.println("Spieler 2 hat gewonnen.");
                //Spieler 2 hat gewonnen
                return true;
            }
            if(!isFirstPlayer && tokenFirst > 32) {
                System.out.println("Spieler 1 hat gewonnen.");
                //Spieler 1 hat gewonnen
                return true;
            }
            if(tokenFirst == tokenSecond) {
                System.out.println("Unentschieden.");
                //Unentschieden
                return true;
            }
        }

        return false;
    }

    /**
     * Bewertet die momentane Spielsituation für einen Spieler.
     * @param forTheFirst Gibt an, ob für den ersten oder zweiten Spieler bewertet werden soll
     * @return int Ein int-Wert der das Spiel bewertet für den gegebenen Spieler
     */
    @Override
    public int evalState(boolean forTheFirst) {
        return countTokens(forTheFirst) - countTokens(!forTheFirst);
    }

    /**
     * Gibt eine ordentliche Text-Repräsentation des Spielbrettes zurück.
     * @return String Text-Repräsentation des Spielfeldes
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        for(byte x = FIELD_BEGIN; x < FIELD_END; x++) {
            for(byte y = FIELD_BEGIN; y < FIELD_END; y++) {
                result.append(board[x][y]);
            }
            result.append("\n");
        }

        return result.toString();
    }

    /**
     * Klont das komplette Reversi-Objekt.
     * @return Object Ein Klon des Reversi-Objekts
     * @throws CloneNotSupportedException Falls etwas beim Aufruf von clone() fehlschlägt
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Reversi copy = (Reversi) super.clone();
        copy.board = new byte[SIZE][SIZE];

        for (byte x = 0; x < SIZE; x++){
            for (byte y = 0; y < SIZE; y++){
                copy.board[x][y] = board[x][y];
            }
        }

        return copy;
    }

    /**
     * Überprüft ob der übergebene Zug nach den Regeln möglich ist.
     * @param x x-Koordinate des Spielzuges
     * @param y y-Koordinate des Spielzuges
     * @return true, wenn der Zug den Regeln entspricht; false, sonst
     */
    public boolean validMove(byte x, byte y) {
        if(curPhase == PHASE_BEGIN) {
            if(x >= MIDDLE_BEGIN && x < MIDDLE_END && y >= MIDDLE_BEGIN && y < MIDDLE_END) {
                return true;
            }
        } else {
            byte token = isFirstPlayer?FIRST_PLAYER:SECOND_PLAYER;
            byte otherToken = isFirstPlayer?SECOND_PLAYER:FIRST_PLAYER;

            for(int deltaX = -1; deltaX <= 1; deltaX++) {
                for(int deltaY = -1; deltaY <= 1; deltaY++) {
                    int tempX = x;
                    int tempY = y;
                    int numFlipped = 0;

                    while(tempX > 0 && tempY > 0 && tempX < FIELD_END && tempY < FIELD_END) {
                        tempX += deltaX;
                        tempY += deltaY;

                        if(board[tempX][tempY] == otherToken) {
                            numFlipped++;
                        }

                        if(board[tempX][tempY] == NO_PLAYER) {
                            break;
                        }

                        if(board[tempX][tempY] == token && numFlipped == 0) {
                            break;
                        }

                        if(board[tempX][tempY] == token && numFlipped > 0) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Zählt die Anzahl der Spielsteine eines Spielers.
     * @param forTheFirst Gibt an, ob für den ersten oder zweiten Spieler gezählt werden soll
     * @return int Anzahl der Spielsteine des übergebenen Spielers
     */
    private int countTokens(boolean forTheFirst) {
        byte token = forTheFirst?FIRST_PLAYER:SECOND_PLAYER;
        int result = 0;

        for(byte y = FIELD_BEGIN; y < FIELD_END; y++) {
            for(byte x = FIELD_BEGIN; x < FIELD_END; x++) {
                if(board[x][y] == token) {
                    result++;
                }
            }
        }

        return result;
    }

    /**
     * Dreht die Spielsteine entsprechend der Regeln um.
     * @param x x-Koordinate des gemachten Zuges
     * @param y y-Koordinate des gemachten Zuges
     */
    private void turnTokens(byte x, byte y) {
        if(curPhase != PHASE_NORMAL) {
            return;
        }

        byte token = isFirstPlayer?FIRST_PLAYER:SECOND_PLAYER;
        byte otherToken = isFirstPlayer?SECOND_PLAYER:FIRST_PLAYER;

        for(int deltaX = -1; deltaX <= 1; deltaX++) {
            for(int deltaY = -1; deltaY <= 1; deltaY++) {
                int tempX = x;
                int tempY = y;
                int numFlipped = 0;

                while(tempX > 0 && tempY > 0 && tempX < FIELD_END && tempY < FIELD_END) {
                    tempX += deltaX;
                    tempY += deltaY;

                    if(board[tempX][tempY] == otherToken) {
                        numFlipped++;
                    }

                    if(board[tempX][tempY] == NO_PLAYER) {
                        break;
                    }

                    if(board[tempX][tempY] == token && numFlipped == 0) {
                        break;
                    }

                    if(board[tempX][tempY] == token && numFlipped > 0) {
                        while(numFlipped > 0) {
                            tempX -= deltaX;
                            tempY -= deltaY;

                            board[tempX][tempY] = token;

                            numFlipped--;
                        }

                        break;
                    }
                }
            }
        }
    }
}
