package de.celthar.reversi;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import name.panitz.pmt2012.AI;
import name.panitz.pmt2012.MinMax;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Ein Objekt dieser Klasse stellt die GUI für ein Reversi-Objekt zur Verfügung
 * und erlaubt die Interaktion mit dem Reversi-Objekt.
 * @author sascha
 */
public class ReversiGui extends JPanel {
    private AI<ReversiMove> ai = new MinMax<>();
    private List<ReversiMove> turnList = new LinkedList<>();
    private boolean aiActive = true;
    private Reversi game;

    /**
     * Der Konstruktor um ein ReversiGui-Objekt zu erstellen.
     * @param game Das Reversi-Objekt, auf dem die GUI aufgebaut ist
     */
    public ReversiGui(Reversi game) {
        byte size = Reversi.FIELD_END - Reversi.FIELD_BEGIN;

        this.game = game;
        this.setLayout(new GridLayout(size, size));

        for (byte x = Reversi.FIELD_BEGIN; x < Reversi.FIELD_END; x++){
            for (byte y = Reversi.FIELD_BEGIN; y < Reversi.FIELD_END; y++){
                this.add(new SingleField(x, y));
            }
        }
    }

    /**
     * Startet eine neue Reversi-Partie.
     * @param aiActive Legt fest, ob mit oder ohne KI gespielt wird
     */
    public void startNewGame(boolean aiActive) {
        this.aiActive = aiActive;
        this.game = new Reversi();
        this.repaint();
    }

    /**
     * Speichert das Spiel als XML-Datei ab. Dazu wird jeder Spielzug in der
     * Form
     * &lt;move&gt;
     * &lt;row>r&lt;/row&gt;
     * &lt;column>c&lt;/column&gt;
     * &lt;/move&gt;
     * abgespeichert. (Nur ein Spielstand möglich!)
     * Die Einstellung der KI wird dabei beachtet.
     */
    public void saveGame() {
        try (Writer out = new BufferedWriter(new FileWriter("savedGame.xml"))) {
            StringBuilder sb = new StringBuilder();

            sb.append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
            sb.append("<turnList aiActive=\"").append(this.aiActive).append("\">\n");

            for(ReversiMove m: turnList) {
                sb.append("\t<move>\n");
                sb.append("\t\t<row>");
                sb.append(m.row);
                sb.append("</row>\n");
                sb.append("\t\t<column>");
                sb.append(m.column);
                sb.append("</column>\n");
                sb.append("\t</move>\n");
            }

            sb.append("</turnList>\n");

            out.write(sb.toString());
            out.close();
        } catch (Exception ex) {
            System.out.println("Failed: Couldn't save your game!");
        }
    }

    /**
     * Lädt ein gespeichertes Spiel aus der XML-Datei.
     */
    public void loadGame() {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document d = db.parse("savedGame.xml");
            Element root = d.getDocumentElement();

            startNewGame(false);

            LinkedList<ReversiMove> loadedMoves = getMovesFromXML(root);

            for(ReversiMove m: loadedMoves) {
                game = game.doMove(m);
            }

            if(root.getAttribute("aiActive").equals("true")) {
                activateAI();
            }

            if(game.isFirstPlayer()) {
                JOptionPane.showMessageDialog(null, "Spieler 1 ist am Zug");
            } else {
                JOptionPane.showMessageDialog(null, "Spieler 2 ist am Zug");
            }
        } catch (ParserConfigurationException ex) {
            System.out.println("Failed: Couldn't load your game!");
            System.out.println("Error: Something is wrong with your Java-XML-Parser!");
        } catch (SAXException ex) {
            System.out.println("Failed: Couldn't load your game!");
            System.out.println("Error: Your savefile is corrupted!");
        } catch (IOException ex) {
            System.out.println("Failed: Couldn't load your game!");
            System.out.println("Error: Couldn't access savedata!");
        }
    }

    /**
     * Gibt die Feldfarbe je nach Belegung an.
     * @param row Reihe des Spielfeldes
     * @param column Spalte des Spielfeldes
     * @return Color Entsprechendes Color-Objekt, je nach Belegung
     */
    private Color getPlayerColor(byte row, byte column) {
        switch(game.board[row][column]) {
            case Reversi.NO_PLAYER:
                if(game.validMove(row, column)) {
                    //Leeres Feld und möglicher Zug
                    return Color.WHITE;
                } else {
                    //Leeres Feld und kein möglicher Zug
                    return Color.GRAY;
                }
            case Reversi.FIRST_PLAYER:
                //Feld von Spieler 1
                return Color.BLACK;
            default:
                //Feld von Spieler 2
                return Color.RED;
        }
    }

    /**
     * Liest die einzelnen Werte der Spielzüge in eine LinkedList&lt;Integer&gt; und
     * rekonstruiert daraus die abgespeicherten Spielzüge in eine
     * LinkedList&lt;ReversiMove&gt;.
     * @param root Der Wurzelknoten des XML-Dokuments
     * @return LinkedList&lt;ReversiMove&gt; Korrekte Abfolge der abgespeicherten Spielzüge
     */
    private static LinkedList<ReversiMove> getMovesFromXML(Node root) {
        LinkedList<Integer> tmp = new LinkedList<>();
        LinkedList<ReversiMove> result = new LinkedList<>();

        NodeList nl = root.getChildNodes();

        for(int i = 0; i < nl.getLength(); i++) {
            Node curItem = nl.item(i);

            if(curItem.getNodeType() == Node.ELEMENT_NODE) {
                NodeList nl2 = curItem.getChildNodes();

                for(int j = 0; j < nl2.getLength(); j++) {
                    Node curItem2 = nl2.item(j);

                    if(curItem2.getNodeType() == Node.ELEMENT_NODE) {
                        tmp.add(Integer.valueOf(curItem2.getTextContent()));
                    }
                }
            }
        }

        for(ListIterator<Integer> i = tmp.listIterator(); i.hasNext();) {
            result.add(new ReversiMove(i.next().byteValue(), i.next().byteValue()));
        }

        return result;
    }

    /**
     * Aktiviert die KI.
     */
    private void activateAI() {
        this.aiActive = true;
    }

    /**
     * Ein Objekt dieser Klasse stellt ein einzelnes Spielfeld dar.
     */
    class SingleField extends JButton {
        byte row;
        byte column;

        /**
         * Der Konstruktor erstellt ein SingleField-Objekt.
         * @param x Die x-Koordinate des Objekts auf dem Spielbrett
         * @param y Die y-Koordinate des Objekts auf dem Spielbrett
         */
        public SingleField(byte x, byte y) {
            super();
            this.row = x;
            this.column = y;
            this.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (!game.otherPlayerHasWon() && game.board[row][column] == Reversi.NO_PLAYER) {
                        ReversiMove tmpMove = new ReversiMove(row, column);

                        if(game.moves().isEmpty()) {
                            tmpMove = null;
                        }

                        if(tmpMove == null || game.validMove(tmpMove.row, tmpMove.column)) {
                            game = game.doMove(tmpMove);
                            turnList.add(tmpMove);

                            if(aiActive) {
                                tmpMove = ai.getBestMove(game);
                                game = game.doMove(tmpMove);
                                turnList.add(tmpMove);
                            }
                        }
                    }

                    getParent().repaint();
                }
            });
        }

        /**
         * Legt die gewünschte Größe eines Feldes fest.
         * @return Dimension Breite: 32 und Höhe: 32
         */
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(32, 32);
        }

        /**
         * Verantwortlich für das Zeichnen des Spielfeldes.
         * @param g Das Graphics-Objekt, auf dem gezeichnet wird
         */
        @Override
        protected void paintComponent(Graphics g) {
            g.setColor(getPlayerColor(row, column));
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
        }
    }

    /**
     * Stellt einen JFrame zur Verfügung, in dem das Spiel gezeichnet und
     * ausgeführt wird.
     * @param args Kommandozeilenparamter (ignoriert)
     */
    public static void main(String[] args) {
        Reversi curGame = new Reversi();
        ReversiGui gui = new ReversiGui(curGame);
        JFrame frame = new JFrame("Reversi");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setJMenuBar(new ReversiMenu(gui));
        frame.add(gui);
        frame.pack();
        frame.setVisible(true);
    }
}
